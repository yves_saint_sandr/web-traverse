from django.db import models

class PrimaryManager(models.Manager):

	@property
	def primary(self):
		primary = self.get_queryset().filter(primary=True).first()
		if not primary:
			if not self.get_queryset():
				return None
			obj = self.get_queryset().last()
			obj.primary = True
			obj.save()
			primary = obj
		return primary

	@primary.setter
	def primary(self, obj):
		map(self._reset, self.get_queryset())
		obj.primary = True
		obj.save()

	def reset(self):
		map(self._reset, self.get_queryset())

	def _reset(self, obj):
		obj.primary = False
		obj.save()

class PaginateManager(models.Manager):
	
	

	def paginate(self, page):
		ITEMS_PER_PAGE = 5
		return self.get_queryset().filter(public=True)[(int(page)-1)*ITEMS_PER_PAGE:int(page)*ITEMS_PER_PAGE]
		