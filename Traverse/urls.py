from django.conf.urls import url, include
from django.contrib import admin
from . import views

urlpatterns = [
	url(r'^$', views.index),
	url(r'^files/$', views.files),
    url(r'^admin/', admin.site.urls),
    url(r'^trashbin/', include('trashbin.urls')),
    url(r'^tasks/', include('tasks.urls')),
    url(r'^', include('users.urls')),
]
