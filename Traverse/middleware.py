from django.http import HttpResponse, JsonResponse, QueryDict
import json
from django.utils.deprecation import MiddlewareMixin

class JSONMiddleware(MiddlewareMixin):

    def process_request(self, request):
        if 'application/json' == request.content_type:
            data = json.loads(request.body)
            
            q_data = QueryDict('', mutable=True)
            q_data.update({"user": request.user.id})
            for key, value in data.iteritems():
                if isinstance(value, list):
                    for x in value:
                        q_data.update({key: x})
                else:
                    q_data.update({key: value})

            if request.method == 'GET':
                request.GET = q_data

            if request.method == 'POST':
                request.POST = q_data