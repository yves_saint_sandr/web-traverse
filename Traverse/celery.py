from __future__ import absolute_import
import os
from celery import Celery
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Traverse.settings')
app = Celery('Traverse')

app.add_defaults({
    'CELERYD_HIJACK_ROOT_LOGGER': False,
})


app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

