from django.http import HttpResponse, JsonResponse, QueryDict
from django.shortcuts import render, redirect
from traverse.models.public import Q
from django.contrib.auth.decorators import login_required
import pprint
import urllib

@login_required(login_url='/login/')
def index(request):
	return render(request, 'main.html')

@login_required(login_url='/login/')
def files(request):
	path = urllib.unquote(request.POST["path"])
	return JsonResponse( Q(path, depth=2).as_json())