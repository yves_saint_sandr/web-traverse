/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 23);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (['$httpProvider', function httpConfig($httpProvider) {
	$httpProvider.defaults.xsrfCookieName = 'csrftoken';
	$httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony default export */ var _unused_webpack_default_export = ([
		'$scope', 
		'close',
		function modalController($scope, close) {
	
}]);

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = ([
		'$scope', 
		'FileService', 
		'TaskService', 
		'BinService',
		'SearchService',
		function traverseController($scope, FileService, TaskService, BinService, SearchService) {
	$scope.FileService = FileService;
	$scope.FileService.getFiles();
	$scope.showLike = "list";

	$scope.TaskService = TaskService;
	$scope.TaskService.nextPage();
	$scope.BinService = BinService;
	$scope.SearchService = SearchService;
	$scope.BinService.getBins();
}]);

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = fileListItem;
function fileListItem() {
	return {
		template: ` <div class="bin-form">
						<div>
							<p>Create a new Bin</p>
							<input 
								type="text" 
								placeholder="Name"
								ng-model="BinService.newBin.name"/>
							<input 
								type="checkbox" 
								class="checkbox"
								id="primary"
								ng-model="BinService.newBin.primary"/>
							<label for="primary">
								Make this Bin Primary
							</label>
						</div>
						<img src="/static/images/add.svg" ng-click="BinService.postBin()" />
					</div>`
	}
}

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (function() {
	return {
		template: ` <bins-list-item 
						ng-class="{primary: BinService.bins[$index].primary}" 
						ng-repeat="bin in BinService.bins"
						ng-animate="'animate'">
	
					</file-list-item>`
	}
});

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = fileGridItem;
function fileGridItem() {
	return {
		template: ` <div>
						<img 
							src="/static/images/garbage.svg"
							ng-click="BinService.update($index)"
						>
						<input ng-show="BinService.redacting[$index]" ng-model="bin.name" ng-readonly="!BinService.redacting[$index]" />
						<div ng-hide="BinService.redacting[$index]" ng-click="BinService.getForm(bin.id)">
							{{ bin.name }}
						</div>
						<input 
							type="radio" 
							id="bin{{$index}}"
							class="radio"
							ng-model="bin.primary"
							ng-value="true"
							ng-change="BinService.makePrimary($index)"
						/>
						<label ng-hide="BinService.redacting[$index]" for="bin{{$index}}" ng-click="BinService.makePrimary($index)" />
						<div
							ng-show="BinService.redacting[$index]"
							ng-click="BinService.remove($index)"
						>
							REMOVE
						</div>

					</div>
					<div class="message" ng-show="!bin.trash.length && bin.primary">There are no trash here yet</div>
					<trash-list-item
						ng-show="bin.primary"
						ng-repeat="trash in bin.trash"
						ng-animate="'animate'"
					></trash-list-item>`
	}
}

/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (function() {
	return {
		template: ` <div class="trash" id="trash-item{{$index}}">
						<div>{{ trash.name | limitTo:10 }}</div>
						<div>{{trash.deleted_at | date: "short" }}</div>
						<div>{{trash.placement}}</div>
						<div>
							<img src="/static/images/restore.svg"
								ng-click="TaskService.restore(trash)"
								id="restore" />
							<img src="/static/images/dispose.svg"
								ng-click="TaskService.dispose(trash)"
								id="dispose" />
						</div>
					</div>`
	}
});

/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = controlPanel;
function controlPanel() {
	return {
		template: ` 
			<div class="control-panel">
				<div class="current-path">
					<span 
						ng-repeat="directory in FileService.breadcrumbs track by $index"
						ng-click="FileService.goBack($index);"
					>
						<a> {{ directory }} </a>/
					</span>
				</div>
				<div>
				
					<input type="checkbox" 
						ng-model="FileService.hidden"
						class="checkbox"
						id="hidden"
					>
						Show hidden
					</input>
					<label for="hidden">
					</label>
					
					<input type="checkbox"
						class="checkbox"
						id="show"
						ng-model="showLike"
						ng-true-value="'list'"
						ng-false-value="'grid'"
					>
						Show like
					</input>
					<label for="show">
					</label>
					<img ng-hide="TaskService.creating" src="/static/images/add.svg" ng-click="TaskService.newTask()"></img>
					<img ng-show="TaskService.creating" src="/static/images/ok.svg" ng-click="TaskService.sendTask()"></img>
				</div>
			</div>
		`
	}
}

/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (function() {
	return {
		template: ` <file-grid-item 
						ng-repeat="file in SearchService.show"  
						ng-class="{selected: FileService.files[$index].selected}"
						ng-animate="'animate'">
					
					</file-grid-item>
					<file-grid-item 
						ng-class="{selected: FileService.files[$index].selected}" 
						hidden-file="true" ng-if="FileService.hidden" 
						ng-repeat="file in SearchService.showHidden"
						ng-animate="'animate'">

					</file-grid-item>`
	}
});

/***/ }),
/* 9 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = fileGridItem;
function fileGridItem() {
	return {
		template: ` <div>
						<input 
							ng-show="TaskService.creating" 
							type="radio"
							id="file{{$index}}"
							class="radio"
							ng-click="TaskService.update($index)"
						></input>
						<label for="file{{$index}}" ng-show="TaskService.creating">
						</label>
						<div ng-click="FileService.goStepForward(file)">
							<img src="/static/images/{{file.type}}.svg">
							{{ file.name }}
						</div>
					</div>`
	}
}

/***/ }),
/* 10 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (function() {
	return {
		template: ` <file-list-item 
						ng-class="{selected: FileService.files[$index].selected}" 
						ng-repeat="file in SearchService.show"
						ng-animate="'animate'">
	
					</file-list-item>
					<file-list-item 
						ng-class="{selected: FileService.files[$index].selected}" 
						hidden-file="true" ng-if="FileService.hidden" 
						ng-repeat="file in SearchService.showHidden"
						ng-animate="'animate'">

					</file-list-item>`
	}
});

/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = fileListItem;
function fileListItem() {
	return {
		template: ` <div>
						<input 
							ng-show="TaskService.creating" 
							type="radio" 
							id="file{{$index}}"
							class="radio"
							ng-click="TaskService.update($index)"
						></input>
						<label for="file{{$index}}" ng-show="TaskService.creating">
						</label>
						<div ng-click="FileService.goStepForward(file)">
							<img src="/static/images/{{file.type}}.svg">
							{{ file.name }}
						</div>
					</div>`
	}
}

/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = modalWindow;
function modalWindow() {
	return {
		template: ` <div 
						ng-if="BinService.modal" 
						class="modal-background"
						ng-click="BinService.closeModal()">
						<div id="modal" ng-click="$event.stopPropagation()">
							SELECT SETTINGS
							<settings-choice></settings-choice>
							<hr/>
							<div ng-show="BinService.isNew()">CREATE NEW SETTINGS</div>
							<settings-form></settings-form>
						</div>
					</div>`
	}
}

/***/ }),
/* 13 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = searchForm;
function searchForm() {
	return {
		template: ` <div class="search-form">
						<input type="text" 
						ng-model="SearchService.pattern"
						ng-change="SearchService.search()"
						placeholder="Search" />
					</div>`
	}
}

/***/ }),
/* 14 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = settingChoice;
function settingChoice() {
	return {
		template: ` <div>
						<select 
							ng-model="BinService.usedSetting"
							ng-options="setting.id as setting.public_name for setting in BinService.settings"
							ng-change="BinService.refreshForm()"
						>

						</select>
					</div>`
	}
}

/***/ }),
/* 15 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = settingForm;
function settingForm() {
	return {
		template: ` <div>
						<input type="text" 
						ng-model="BinService.settingsForm.name"
						placeholder="Name" 
						ng-show="BinService.isNew()" />

						<div>
							<input type="checkbox" 
							id="settings_primary"
							class="checkbox"
							ng-model="BinService.settingsForm.primary" />
							<label for="settings_primary">Primary</label>

							<input type="checkbox" 
							id="settings_silent"
							class="checkbox"
							ng-model="BinService.settingsForm.silent" />
							<label for="settings_silent">Silent</label>
						</div>

						<div>
							<input type="checkbox" 
							id="settings_force"
							class="checkbox"
							ng-model="BinService.settingsForm.force" />
							<label for="settings_force">Force</label>

							<input type="checkbox" 
							id="settings_dummy"
							class="checkbox"
							ng-model="BinService.settingsForm.dummy" />
							<label for="settings_dummy">Dummy</label>
						</div>

						<button ng-click="BinService.submitForm()">OK</button>
					</div>`
	}
}

/***/ }),
/* 16 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (function() {
	return {
		template: ` <div infinite-scroll='TaskService.nextPage()' infinite-scroll-disabled='TaskService.busy || TaskService.stop' infinite-scroll-distance='1'>
						<task-list-item 
							ng-class="{{ task.status.toLowerCase() }}" 
							ng-repeat="task in TaskService.tasks"
							ng-animate="'animate'">
		
						</task-list-item>
					</div>
					<div ng-show='TaskService.busy' class="spinner"></div>
				  `
	}
});

/***/ }),
/* 17 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = fileListItem;
function fileListItem() {
	return {
		template: ` <div>
						<figure><img src="/static/images/task_{{ task.action }}.svg" />
						<figcaption>{{ task.action }}</figcaption> </figure>
						<div><div>{{ task.file_name | limitTo: "40" }} </div>
						<div>{{ task.created_at | date: "short" }} </div></div>
						<figure>
							<img src="/static/images/status_{{ task.status.toLowerCase() }}.svg" />
							<figcaption><div>{{ task.status.toLowerCase() }}</div>
								<a 
									ng-if="TaskService.canCancel(task.status)"
									ng-click="TaskService.cancel(task.id)">
									Cancel
								</a>
							</figcaption> 
						</figure>
					</div>`
	}
}

/***/ }),
/* 18 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (['$http', 'TrashService', function taskService($http, TrashService) {
	const that = this;

	that.bins = [];
	that.TrashService = TrashService;

	that.settingsForm = {};
	that.usedSetting = Infinity;
	that.currentBin = undefined;
	that.settings = []

	that.redacting = []

	that.modal = false;

	that.empty = true;
	that.newBin = {
		name: "",
		max_size: 1024,
		primary: false,
	}
	that.primaryBin = undefined;

	that.initByResponse = function(response) {
		that.bins = response.data;
		that.empty = that.bins.length == 0;
		that.primaryBin = that.bins.filter(function(bin) { 
			return bin.primary == true;
		})[0];
		that.selected = that.bins.map(function(e){return false;})
		that.select(that.bins.indexOf(that.primaryBin));
	}

	that.getBins = function() {
		return $http({
			method: "get",
			url: "/trashbin/bins/"
		}).then(function(response){
			that.initByResponse(response);
		})	
	}

	that.postBin = function() {
		return $http({
			method: "post",
			data: that.newBin,
			url: "/trashbin/new/"
		}).then(function(response) {
			that.newBin = {
				name: "",
				max_size: 1024
			}
			that.getBins();
		})
	}


	that.makePrimary = function($index) {
		that.primaryBin.primary = false;
		that.bins[$index].primary = true;
		that.changeCheckboxState($index, that.bins.indexOf(that.primaryBin));
		return $http({
			method: "post",
			data: {
				new: that.bins[$index],
				prev: that.primaryBin
			},
			url: "/trashbin/change/"
		}).then(function(response){
			that.initByResponse(response);
			that.select($index);
		})

	}

	that.changeCheckboxState = function(first, second){
		document.getElementById('bin'+first).checked = true;
		document.getElementById('bin'+second).checked = false;
	}



	that.select = function($index) {
		if (that.selected[$index]){
			that.selected[$index] = false;
			that.bins[$index].trash = [];
		} else {
			that.selected[$index] = true;
			that.TrashService.getTrash(that.bins[$index].name).then(function(response){
				that.bins[$index].trash = response.data;
			});


		}
		console.log(that.bins[$index].trash);
	}

	that.removeTrash = function(trash) {
		that.primaryBin.trash = that.primaryBin.trash.filter(function(el){
			return el.descriptor != trash.descriptor;
		});
	}

	that.getForm = function(id) {
		that.currentBin = id;
		return $http({
			method: "get",
			url: "/trashbin/" + id + "/settings/"
		}).then(function(response){
			console.log(response.data);
			that.usedSetting = response.data.filter(function(e) { 
				return e.primary 
			})[0].id;
			that.settings = response.data.map(function(e){ 
				e.public_name = e.name; 
				return e;
			});
			that.settings.unshift({
				public_name: "~/~ Create New One ~/~",
				id: undefined,
			})
			that.refreshForm();
			that.showModal();
		})
	}



	that.refreshForm = function() {
		that.settingsForm = that.settings.filter(function(e){
			return e.id == that.usedSetting;
		})[0];
	}

	that.showModal = function() {
		that.modal = true;
	}

	that.closeModal = function(){
		that.modal = false;
	}

	that.isNew = function(){
		return that.settingsForm.id == undefined;
	}

	that.submitForm = function() {
		that.settingsForm.trashbin = that.currentBin;
		return $http({
			method: "post",
			data: {
				selected: that.usedSetting,
				form: that.settingsForm,
			},
			url: "/trashbin/" + that.currentBin + "/settings/"
		}).then(function(response){
			that.settingsForm = {};
			console.log(response.data);
			that.closeModal();
		})
	}

	that.update = function($index) {
		if (that.redacting[$index]) {
			return $http({
				method: "post",
				data: {
					"name": that.bins[$index].name
				},
				url: "/trashbin/" + that.bins[$index].id + "/update/"
			}).then(function(response){
				that.bins[$index].name = response.data["name"];
				that.redacting[$index] = false;
			})
			
		} else {
			that.redacting[$index] = true;
		}
	}

	that.remove = function($index) {
		return $http({
			method: "post",
			url: "/trashbin/" + that.bins[$index].id + "/remove/"

		}).then(function(response){
			that.bins.splice($index, 1);
		})
	}

}]);

/***/ }),
/* 19 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (['$http', 'SearchService', function fileService($http, SearchService) {
	const that = this;

	that.currentDirectory = "~";
	that.files = [];
	that.hiddenFiles = [];
	that.breadcrumbs = [];
	that.showHidden = false;
	that.ready = true;
	that.cache = {};

	that.priorities = {
		"directory": 0,
		"file": 1,
		"link": 2
	}

	that.compare = function(first, second) {
		if (that.priorities[first.type] == that.priorities[second.type]) {
			//console.log(first.name[0] + " " + second.name[0]);
			return first.name[0] <= second.name[0];
		} else {
			//console.log(that.priorities[first.type] + " " + that.priorities[second.type]);
			return that.priorities[first.type] - that.priorities[second.type];
		}
	}

	that.sort = function() {
		that.files.sort(that.compare);
		that.hiddenFiles.sort(that.compare);
	}

	that.getFiles = function() {
		that.breadcrumbs = that.currentDirectory.split("/");
		that.ready = false;
		if (that.cache.hasOwnProperty(that.currentDirectory)) {
			that.files = that.cache[that.currentDirectory][0].slice();
			that.hiddenFiles = that.cache[that.currentDirectory][1].slice();
			SearchService.init(that.files, that.hiddenFiles);
		} else {
			return $http({
				method: "post",
				url: "/files/",
				data: {
					path: encodeURIComponent(that.currentDirectory)
				}
			}).then(function(response){
				that.files = [];
				that.hiddenFiles = [];
				for (var i = 0; i < response.data.content.length; i++) {
					if (response.data.content[i]["name"].startsWith(".")){
						that.hiddenFiles.push(response.data.content[i]);
					} else {
						that.files.push(response.data.content[i]);
					}
				}
				that.sort();
				SearchService.init(that.files, that.hiddenFiles);
				that.ready = true;
			})
		}
	}

	that.goBack = function($index) {
		console.log();
		that.breadcrumbs = that.breadcrumbs.slice(0, $index+1);
		that.currentDirectory = that.breadcrumbs.join("/");
		console.log(that.currentDirectory);
		that.getFiles();
	}

	that.goStepForward = function(file) {
		if (!file.selected && file.type == "directory") {
			that.cache[that.currentDirectory] = [
				that.files.slice(),
				that.hiddenFiles.slice()
			]
			that.currentDirectory += "/" + file.name;
			that.breadcrumbs = that.currentDirectory.split("/");
			that.getFiles();
		}
	}

	that.remove = function(removed){
		that.files = that.files.filter(function(el){
			return removed.indexOf(el) < 0;
		});
		that.hiddenFiles = that.hiddenFiles.filter(function(el){
			return removed.indexOf(el) < 0;
		});
		SearchService.init(that.files, that.hiddenFiles);
	}
}]);

/***/ }),
/* 20 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = searchService;
function searchService() {
	const that = this;

	that.files= [];
	that.hiddenFiles = [];
	that.show = []; 
	that.showHidden = [];
	that.pattern = "";

	that.init = function(files, hidden){
		that.files= files;
		that.hiddenFiles = hidden;
		that.show = files;
		that.showHidden = hidden;
	}

	that.search = function() {
		that.show = that.files.filter(function(file){
			return file.name.startsWith(that.pattern);
		})
		that.showHidden = that.hiddenFiles.filter(function(file){
			return file.name.startsWith(that.pattern);
		})
	}

}

/***/ }),
/* 21 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (['$http', 'FileService', 'BinService', function taskService($http, FileService, BinService) {
	const that = this;

	that.FileService = FileService;
	that.BinService = BinService;
	that.selected = [];
	that.creating = false;
	that.tasks = [];
	that.empty = true;
	that.busy = false;
	that.currentPage = 1;

	that.newTask = function(){
		that.creating = true;
	}

	that.update = function($index) {
		var file = FileService.files[$index];
		console.log(file);
		if (that.selected.includes(file)){
			var index = that.selected.indexOf(file);
			that.selected.splice(index, 1);
			file.selected = false;
			document.getElementById('file'+$index).checked = false;
		} else {	
			that.selected.push(file);
			file.selected = true;
		}
	}

	that.sendTask = function() {
		that.creating = false;
		return $http({
			method: "post",
			data: {
				"files" : that.selected,
				"length" : that.selected.length
			},
			url: "/tasks/new/"
		}).then(function(response){
			FileService.remove(that.selected);
			that.selected = [];

			alert("Done");
		})
	}


	that.restore = function(trash) {
		return $http({
			method: "post",
			data: {
				"descriptor": trash.descriptor,
			},
			url: "/tasks/restore/"
		}).then(function(response){
			BinService.removeTrash(trash);
			alert("Done");
		})
	}

	that.dispose = function(trash) {
		return $http({
			method: "post",
			data: {
				"descriptor" : trash.descriptor,
			},
			url: "/tasks/dispose/"
		}).then(function(response){
			BinService.removeTrash(trash);
			alert("Done");
		})
	}

	that.nextPage = function() {
		that.busy = true;
		return $http({
			method: "get",
			url: "/tasks/" + that.currentPage + "/"
		}).then(function(response){
			that.tasks = that.tasks.concat(response.data);
			
			that.currentPage++;
			that.empty = that.tasks.length == 0;
			that.busy = false;
			if (response.data.length != 5)
				that.stop = true;
			console.log(response.data.length);
		})
	}

	that.canCancel = function(status) {
		return status != "FAILURE" && status != "SUCCESS" && status != "FORGOT";
	}

	that.cancel = function(id) {
		return $http({
			method: "post",
			url: "/tasks/" + id + "/cancel/"
		}).then(function(response){
			console.log(response.data);
			that.tasks = response.data;
			that.empty = that.tasks.length == 0;
		})
	}

}]);

/***/ }),
/* 22 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = (['$http', function taskService($http) {
	const that = this;

	that.getTrash = function(name) {
		return $http({
			method: "get",
			url: "/tasks/inspect/" + name + "/",
		})
	}
}]);

/***/ }),
/* 23 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__configs_httpConfig__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__controllers_traverseController__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__controllers_modalController__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_fileService__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_taskService__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_trashService__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_binService__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_searchService__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__directives_list_fileList__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__directives_list_fileListItem__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__directives_tasks_taskList__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__directives_tasks_taskListItem__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__directives_bins_binsList__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__directives_bins_binsListItem__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__directives_bins_trashListItem__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__directives_grid_fileGrid__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__directives_grid_fileGridItem__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__directives_controlPanel__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__directives_binForm__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__directives_searchForm__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__directives_settingsForm__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__directives_settingsChoice__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__directives_modalWindow__ = __webpack_require__(12);
































const app = angular.module("TraverseApp", ['ngAnimate', 'infinite-scroll'])

app
.config(__WEBPACK_IMPORTED_MODULE_0__configs_httpConfig__["a" /* default */])

.controller("TraverseController", __WEBPACK_IMPORTED_MODULE_1__controllers_traverseController__["a" /* default */])

.service("FileService", __WEBPACK_IMPORTED_MODULE_3__services_fileService__["a" /* default */])
.service("TaskService", __WEBPACK_IMPORTED_MODULE_4__services_taskService__["a" /* default */])
.service("TrashService", __WEBPACK_IMPORTED_MODULE_5__services_trashService__["a" /* default */])
.service("BinService", __WEBPACK_IMPORTED_MODULE_6__services_binService__["a" /* default */])
.service("SearchService", __WEBPACK_IMPORTED_MODULE_7__services_searchService__["a" /* default */])

.directive("fileList", __WEBPACK_IMPORTED_MODULE_8__directives_list_fileList__["a" /* default */])
.directive("fileListItem", __WEBPACK_IMPORTED_MODULE_9__directives_list_fileListItem__["a" /* default */])
.directive("fileGrid", __WEBPACK_IMPORTED_MODULE_15__directives_grid_fileGrid__["a" /* default */])
.directive("fileGridItem", __WEBPACK_IMPORTED_MODULE_16__directives_grid_fileGridItem__["a" /* default */])
.directive("binsList", __WEBPACK_IMPORTED_MODULE_12__directives_bins_binsList__["a" /* default */])
.directive("binsListItem", __WEBPACK_IMPORTED_MODULE_13__directives_bins_binsListItem__["a" /* default */])
.directive("taskList", __WEBPACK_IMPORTED_MODULE_10__directives_tasks_taskList__["a" /* default */])
.directive("taskListItem", __WEBPACK_IMPORTED_MODULE_11__directives_tasks_taskListItem__["a" /* default */])
.directive("trashListItem", __WEBPACK_IMPORTED_MODULE_14__directives_bins_trashListItem__["a" /* default */])

.directive("modalWindow", __WEBPACK_IMPORTED_MODULE_22__directives_modalWindow__["a" /* default */])
.directive("controlPanel", __WEBPACK_IMPORTED_MODULE_17__directives_controlPanel__["a" /* default */])
.directive("binForm", __WEBPACK_IMPORTED_MODULE_18__directives_binForm__["a" /* default */])
.directive("searchForm", __WEBPACK_IMPORTED_MODULE_19__directives_searchForm__["a" /* default */])
.directive("settingsForm", __WEBPACK_IMPORTED_MODULE_20__directives_settingsForm__["a" /* default */])
.directive("settingsChoice", __WEBPACK_IMPORTED_MODULE_21__directives_settingsChoice__["a" /* default */])



/***/ })
/******/ ]);