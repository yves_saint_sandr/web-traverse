export default function searchService() {
	const that = this;

	that.files= [];
	that.hiddenFiles = [];
	that.show = []; 
	that.showHidden = [];
	that.pattern = "";

	that.init = function(files, hidden){
		that.files= files;
		that.hiddenFiles = hidden;
		that.show = files;
		that.showHidden = hidden;
	}

	that.search = function() {
		that.show = that.files.filter(function(file){
			return file.name.startsWith(that.pattern);
		})
		that.showHidden = that.hiddenFiles.filter(function(file){
			return file.name.startsWith(that.pattern);
		})
	}

}