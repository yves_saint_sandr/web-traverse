export default ['$http', 'SearchService', function fileService($http, SearchService) {
	const that = this;

	that.currentDirectory = "~";
	that.files = [];
	that.hiddenFiles = [];
	that.breadcrumbs = [];
	that.showHidden = false;
	that.ready = true;
	that.cache = {};

	that.priorities = {
		"directory": 0,
		"file": 1,
		"link": 2
	}

	that.compare = function(first, second) {
		if (that.priorities[first.type] == that.priorities[second.type]) {
			//console.log(first.name[0] + " " + second.name[0]);
			return first.name[0] <= second.name[0];
		} else {
			//console.log(that.priorities[first.type] + " " + that.priorities[second.type]);
			return that.priorities[first.type] - that.priorities[second.type];
		}
	}

	that.sort = function() {
		that.files.sort(that.compare);
		that.hiddenFiles.sort(that.compare);
	}

	that.getFiles = function() {
		that.breadcrumbs = that.currentDirectory.split("/");
		that.ready = false;
		if (that.cache.hasOwnProperty(that.currentDirectory)) {
			that.files = that.cache[that.currentDirectory][0].slice();
			that.hiddenFiles = that.cache[that.currentDirectory][1].slice();
			SearchService.init(that.files, that.hiddenFiles);
		} else {
			return $http({
				method: "post",
				url: "/files/",
				data: {
					path: encodeURIComponent(that.currentDirectory)
				}
			}).then(function(response){
				that.files = [];
				that.hiddenFiles = [];
				for (var i = 0; i < response.data.content.length; i++) {
					if (response.data.content[i]["name"].startsWith(".")){
						that.hiddenFiles.push(response.data.content[i]);
					} else {
						that.files.push(response.data.content[i]);
					}
				}
				that.sort();
				SearchService.init(that.files, that.hiddenFiles);
				that.ready = true;
			})
		}
	}

	that.goBack = function($index) {
		console.log();
		that.breadcrumbs = that.breadcrumbs.slice(0, $index+1);
		that.currentDirectory = that.breadcrumbs.join("/");
		console.log(that.currentDirectory);
		that.getFiles();
	}

	that.goStepForward = function(file) {
		if (!file.selected && file.type == "directory") {
			that.cache[that.currentDirectory] = [
				that.files.slice(),
				that.hiddenFiles.slice()
			]
			that.currentDirectory += "/" + file.name;
			that.breadcrumbs = that.currentDirectory.split("/");
			that.getFiles();
		}
	}

	that.remove = function(removed){
		that.files = that.files.filter(function(el){
			return removed.indexOf(el) < 0;
		});
		that.hiddenFiles = that.hiddenFiles.filter(function(el){
			return removed.indexOf(el) < 0;
		});
		SearchService.init(that.files, that.hiddenFiles);
	}
}]