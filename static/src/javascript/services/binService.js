export default ['$http', 'TrashService', function taskService($http, TrashService) {
	const that = this;

	that.bins = [];
	that.TrashService = TrashService;

	that.settingsForm = {};
	that.usedSetting = Infinity;
	that.currentBin = undefined;
	that.settings = []

	that.redacting = []

	that.modal = false;

	that.empty = true;
	that.newBin = {
		name: "",
		max_size: 1024,
		primary: false,
	}
	that.primaryBin = undefined;

	that.initByResponse = function(response) {
		that.bins = response.data;
		that.empty = that.bins.length == 0;
		that.primaryBin = that.bins.filter(function(bin) { 
			return bin.primary == true;
		})[0];
		that.selected = that.bins.map(function(e){return false;})
		that.select(that.bins.indexOf(that.primaryBin));
	}

	that.getBins = function() {
		return $http({
			method: "get",
			url: "/trashbin/bins/"
		}).then(function(response){
			that.initByResponse(response);
		})	
	}

	that.postBin = function() {
		return $http({
			method: "post",
			data: that.newBin,
			url: "/trashbin/new/"
		}).then(function(response) {
			that.newBin = {
				name: "",
				max_size: 1024
			}
			that.getBins();
		})
	}


	that.makePrimary = function($index) {
		that.primaryBin.primary = false;
		that.bins[$index].primary = true;
		that.changeCheckboxState($index, that.bins.indexOf(that.primaryBin));
		return $http({
			method: "post",
			data: {
				new: that.bins[$index],
				prev: that.primaryBin
			},
			url: "/trashbin/change/"
		}).then(function(response){
			that.initByResponse(response);
			that.select($index);
		})

	}

	that.changeCheckboxState = function(first, second){
		document.getElementById('bin'+first).checked = true;
		document.getElementById('bin'+second).checked = false;
	}



	that.select = function($index) {
		if (that.selected[$index]){
			that.selected[$index] = false;
			that.bins[$index].trash = [];
		} else {
			that.selected[$index] = true;
			that.TrashService.getTrash(that.bins[$index].name).then(function(response){
				that.bins[$index].trash = response.data;
			});


		}
		console.log(that.bins[$index].trash);
	}

	that.removeTrash = function(trash) {
		that.primaryBin.trash = that.primaryBin.trash.filter(function(el){
			return el.descriptor != trash.descriptor;
		});
	}

	that.getForm = function(id) {
		that.currentBin = id;
		return $http({
			method: "get",
			url: "/trashbin/" + id + "/settings/"
		}).then(function(response){
			console.log(response.data);
			that.usedSetting = response.data.filter(function(e) { 
				return e.primary 
			})[0].id;
			that.settings = response.data.map(function(e){ 
				e.public_name = e.name; 
				return e;
			});
			that.settings.unshift({
				public_name: "~/~ Create New One ~/~",
				id: undefined,
			})
			that.refreshForm();
			that.showModal();
		})
	}



	that.refreshForm = function() {
		that.settingsForm = that.settings.filter(function(e){
			return e.id == that.usedSetting;
		})[0];
	}

	that.showModal = function() {
		that.modal = true;
	}

	that.closeModal = function(){
		that.modal = false;
	}

	that.isNew = function(){
		return that.settingsForm.id == undefined;
	}

	that.submitForm = function() {
		that.settingsForm.trashbin = that.currentBin;
		return $http({
			method: "post",
			data: {
				selected: that.usedSetting,
				form: that.settingsForm,
			},
			url: "/trashbin/" + that.currentBin + "/settings/"
		}).then(function(response){
			that.settingsForm = {};
			console.log(response.data);
			that.closeModal();
		})
	}

	that.update = function($index) {
		if (that.redacting[$index]) {
			return $http({
				method: "post",
				data: {
					"name": that.bins[$index].name
				},
				url: "/trashbin/" + that.bins[$index].id + "/update/"
			}).then(function(response){
				that.bins[$index].name = response.data["name"];
				that.redacting[$index] = false;
			})
			
		} else {
			that.redacting[$index] = true;
		}
	}

	that.remove = function($index) {
		return $http({
			method: "post",
			url: "/trashbin/" + that.bins[$index].id + "/remove/"

		}).then(function(response){
			that.bins.splice($index, 1);
		})
	}

}]