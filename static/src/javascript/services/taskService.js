export default ['$http', 'FileService', 'BinService', function taskService($http, FileService, BinService) {
	const that = this;

	that.FileService = FileService;
	that.BinService = BinService;
	that.selected = [];
	that.creating = false;
	that.tasks = [];
	that.empty = true;
	that.busy = false;
	that.currentPage = 1;

	that.newTask = function(){
		that.creating = true;
	}

	that.update = function($index) {
		var file = FileService.files[$index];
		console.log(file);
		if (that.selected.includes(file)){
			var index = that.selected.indexOf(file);
			that.selected.splice(index, 1);
			file.selected = false;
			document.getElementById('file'+$index).checked = false;
		} else {	
			that.selected.push(file);
			file.selected = true;
		}
	}

	that.sendTask = function() {
		that.creating = false;
		return $http({
			method: "post",
			data: {
				"files" : that.selected,
				"length" : that.selected.length
			},
			url: "/tasks/new/"
		}).then(function(response){
			FileService.remove(that.selected);
			that.selected = [];

			alert("Done");
		})
	}


	that.restore = function(trash) {
		return $http({
			method: "post",
			data: {
				"descriptor": trash.descriptor,
			},
			url: "/tasks/restore/"
		}).then(function(response){
			BinService.removeTrash(trash);
			alert("Done");
		})
	}

	that.dispose = function(trash) {
		return $http({
			method: "post",
			data: {
				"descriptor" : trash.descriptor,
			},
			url: "/tasks/dispose/"
		}).then(function(response){
			BinService.removeTrash(trash);
			alert("Done");
		})
	}

	that.nextPage = function() {
		that.busy = true;
		return $http({
			method: "get",
			url: "/tasks/" + that.currentPage + "/"
		}).then(function(response){
			that.tasks = that.tasks.concat(response.data);
			
			that.currentPage++;
			that.empty = that.tasks.length == 0;
			that.busy = false;
			if (response.data.length != 5)
				that.stop = true;
			console.log(response.data.length);
		})
	}

	that.canCancel = function(status) {
		return status != "FAILURE" && status != "SUCCESS" && status != "FORGOT";
	}

	that.cancel = function(id) {
		return $http({
			method: "post",
			url: "/tasks/" + id + "/cancel/"
		}).then(function(response){
			console.log(response.data);
			that.tasks = response.data;
			that.empty = that.tasks.length == 0;
		})
	}

}]