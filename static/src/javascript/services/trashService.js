export default ['$http', function taskService($http) {
	const that = this;

	that.getTrash = function(name) {
		return $http({
			method: "get",
			url: "/tasks/inspect/" + name + "/",
		})
	}
}]