export default function modalWindow() {
	return {
		template: ` <div 
						ng-if="BinService.modal" 
						class="modal-background"
						ng-click="BinService.closeModal()">
						<div id="modal" ng-click="$event.stopPropagation()">
							SELECT SETTINGS
							<settings-choice></settings-choice>
							<hr/>
							<div ng-show="BinService.isNew()">CREATE NEW SETTINGS</div>
							<settings-form></settings-form>
						</div>
					</div>`
	}
}