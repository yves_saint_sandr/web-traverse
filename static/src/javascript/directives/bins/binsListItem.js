export default function fileGridItem() {
	return {
		template: ` <div>
						<img 
							src="/static/images/garbage.svg"
							ng-click="BinService.update($index)"
						>
						<input ng-show="BinService.redacting[$index]" ng-model="bin.name" ng-readonly="!BinService.redacting[$index]" />
						<div ng-hide="BinService.redacting[$index]" ng-click="BinService.getForm(bin.id)">
							{{ bin.name }}
						</div>
						<input 
							type="radio" 
							id="bin{{$index}}"
							class="radio"
							ng-model="bin.primary"
							ng-value="true"
							ng-change="BinService.makePrimary($index)"
						/>
						<label ng-hide="BinService.redacting[$index]" for="bin{{$index}}" ng-click="BinService.makePrimary($index)" />
						<div
							ng-show="BinService.redacting[$index]"
							ng-click="BinService.remove($index)"
						>
							REMOVE
						</div>

					</div>
					<div class="message" ng-show="!bin.trash.length && bin.primary">There are no trash here yet</div>
					<trash-list-item
						ng-show="bin.primary"
						ng-repeat="trash in bin.trash"
						ng-animate="'animate'"
					></trash-list-item>`
	}
}