export default function() {
	return {
		template: ` <div class="trash" id="trash-item{{$index}}">
						<div>{{ trash.name | limitTo:10 }}</div>
						<div>{{trash.deleted_at | date: "short" }}</div>
						<div>{{trash.placement}}</div>
						<div>
							<img src="/static/images/restore.svg"
								ng-click="TaskService.restore(trash)"
								id="restore" />
							<img src="/static/images/dispose.svg"
								ng-click="TaskService.dispose(trash)"
								id="dispose" />
						</div>
					</div>`
	}
}