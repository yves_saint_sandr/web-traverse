export default function searchForm() {
	return {
		template: ` <div class="search-form">
						<input type="text" 
						ng-model="SearchService.pattern"
						ng-change="SearchService.search()"
						placeholder="Search" />
					</div>`
	}
}