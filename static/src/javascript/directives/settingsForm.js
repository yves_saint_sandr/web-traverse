export default function settingForm() {
	return {
		template: ` <div>
						<input type="text" 
						ng-model="BinService.settingsForm.name"
						placeholder="Name" 
						ng-show="BinService.isNew()" />

						<div>
							<input type="checkbox" 
							id="settings_primary"
							class="checkbox"
							ng-model="BinService.settingsForm.primary" />
							<label for="settings_primary">Primary</label>

							<input type="checkbox" 
							id="settings_silent"
							class="checkbox"
							ng-model="BinService.settingsForm.silent" />
							<label for="settings_silent">Silent</label>
						</div>

						<div>
							<input type="checkbox" 
							id="settings_force"
							class="checkbox"
							ng-model="BinService.settingsForm.force" />
							<label for="settings_force">Force</label>

							<input type="checkbox" 
							id="settings_dummy"
							class="checkbox"
							ng-model="BinService.settingsForm.dummy" />
							<label for="settings_dummy">Dummy</label>
						</div>

						<button ng-click="BinService.submitForm()">OK</button>
					</div>`
	}
}