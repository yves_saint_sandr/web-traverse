export default function fileListItem() {
	return {
		template: ` <div class="bin-form">
						<div>
							<p>Create a new Bin</p>
							<input 
								type="text" 
								placeholder="Name"
								ng-model="BinService.newBin.name"/>
							<input 
								type="checkbox" 
								class="checkbox"
								id="primary"
								ng-model="BinService.newBin.primary"/>
							<label for="primary">
								Make this Bin Primary
							</label>
						</div>
						<img src="/static/images/add.svg" ng-click="BinService.postBin()" />
					</div>`
	}
}