export default function settingChoice() {
	return {
		template: ` <div>
						<select 
							ng-model="BinService.usedSetting"
							ng-options="setting.id as setting.public_name for setting in BinService.settings"
							ng-change="BinService.refreshForm()"
						>

						</select>
					</div>`
	}
}