export default function fileListItem() {
	return {
		template: ` <div>
						<figure><img src="/static/images/task_{{ task.action }}.svg" />
						<figcaption>{{ task.action }}</figcaption> </figure>
						<div><div>{{ task.file_name | limitTo: "40" }} </div>
						<div>{{ task.created_at | date: "short" }} </div></div>
						<figure>
							<img src="/static/images/status_{{ task.status.toLowerCase() }}.svg" />
							<figcaption><div>{{ task.status.toLowerCase() }}</div>
								<a 
									ng-if="TaskService.canCancel(task.status)"
									ng-click="TaskService.cancel(task.id)">
									Cancel
								</a>
							</figcaption> 
						</figure>
					</div>`
	}
}