export default function() {
	return {
		template: ` <div infinite-scroll='TaskService.nextPage()' infinite-scroll-disabled='TaskService.busy || TaskService.stop' infinite-scroll-distance='1'>
						<task-list-item 
							ng-class="{{ task.status.toLowerCase() }}" 
							ng-repeat="task in TaskService.tasks"
							ng-animate="'animate'">
		
						</task-list-item>
					</div>
					<div ng-show='TaskService.busy' class="spinner"></div>
				  `
	}
}