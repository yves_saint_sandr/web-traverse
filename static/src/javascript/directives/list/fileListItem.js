export default function fileListItem() {
	return {
		template: ` <div>
						<input 
							ng-show="TaskService.creating" 
							type="radio" 
							id="file{{$index}}"
							class="radio"
							ng-click="TaskService.update($index)"
						></input>
						<label for="file{{$index}}" ng-show="TaskService.creating">
						</label>
						<div ng-click="FileService.goStepForward(file)">
							<img src="/static/images/{{file.type}}.svg">
							{{ file.name }}
						</div>
					</div>`
	}
}