export default function() {
	return {
		template: ` <file-list-item 
						ng-class="{selected: FileService.files[$index].selected}" 
						ng-repeat="file in SearchService.show"
						ng-animate="'animate'">
	
					</file-list-item>
					<file-list-item 
						ng-class="{selected: FileService.files[$index].selected}" 
						hidden-file="true" ng-if="FileService.hidden" 
						ng-repeat="file in SearchService.showHidden"
						ng-animate="'animate'">

					</file-list-item>`
	}
}