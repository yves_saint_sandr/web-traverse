export default function() {
	return {
		template: ` <file-grid-item 
						ng-repeat="file in SearchService.show"  
						ng-class="{selected: FileService.files[$index].selected}"
						ng-animate="'animate'">
					
					</file-grid-item>
					<file-grid-item 
						ng-class="{selected: FileService.files[$index].selected}" 
						hidden-file="true" ng-if="FileService.hidden" 
						ng-repeat="file in SearchService.showHidden"
						ng-animate="'animate'">

					</file-grid-item>`
	}
}