export default function controlPanel() {
	return {
		template: ` 
			<div class="control-panel">
				<div class="current-path">
					<span 
						ng-repeat="directory in FileService.breadcrumbs track by $index"
						ng-click="FileService.goBack($index);"
					>
						<a> {{ directory }} </a>/
					</span>
				</div>
				<div>
				
					<input type="checkbox" 
						ng-model="FileService.hidden"
						class="checkbox"
						id="hidden"
					>
						Show hidden
					</input>
					<label for="hidden">
					</label>
					
					<input type="checkbox"
						class="checkbox"
						id="show"
						ng-model="showLike"
						ng-true-value="'list'"
						ng-false-value="'grid'"
					>
						Show like
					</input>
					<label for="show">
					</label>
					<img ng-hide="TaskService.creating" src="/static/images/add.svg" ng-click="TaskService.newTask()"></img>
					<img ng-show="TaskService.creating" src="/static/images/ok.svg" ng-click="TaskService.sendTask()"></img>
				</div>
			</div>
		`
	}
}