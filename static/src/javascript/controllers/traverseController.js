export default [
		'$scope', 
		'FileService', 
		'TaskService', 
		'BinService',
		'SearchService',
		function traverseController($scope, FileService, TaskService, BinService, SearchService) {
	$scope.FileService = FileService;
	$scope.FileService.getFiles();
	$scope.showLike = "list";

	$scope.TaskService = TaskService;
	$scope.TaskService.nextPage();
	$scope.BinService = BinService;
	$scope.SearchService = SearchService;
	$scope.BinService.getBins();
}]