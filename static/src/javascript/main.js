import httpConfig from './configs/httpConfig';

import traverseController from './controllers/traverseController';
import modalController from './controllers/modalController';

import fileService from './services/fileService';
import taskService from './services/taskService';
import trashService from './services/trashService';
import binService from './services/binService';
import searchService from './services/searchService';

import fileList from './directives/list/fileList';
import fileListItem from './directives/list/fileListItem';

import taskList from './directives/tasks/taskList';
import taskListItem from './directives/tasks/taskListItem';

import binsList from './directives/bins/binsList';
import binsListItem from './directives/bins/binsListItem';
import trashListItem from './directives/bins/trashListItem';


import fileGrid from './directives/grid/fileGrid';
import fileGridItem from './directives/grid/fileGridItem';

import controlPanel from './directives/controlPanel';
import binForm from './directives/binForm';
import searchForm from './directives/searchForm';
import settingsForm from './directives/settingsForm';
import settingsChoice from './directives/settingsChoice';
import modalWindow from './directives/modalWindow';

const app = angular.module("TraverseApp", ['ngAnimate', 'infinite-scroll'])

app
.config(httpConfig)

.controller("TraverseController", traverseController)

.service("FileService", fileService)
.service("TaskService", taskService)
.service("TrashService", trashService)
.service("BinService", binService)
.service("SearchService", searchService)

.directive("fileList", fileList)
.directive("fileListItem", fileListItem)
.directive("fileGrid", fileGrid)
.directive("fileGridItem", fileGridItem)
.directive("binsList", binsList)
.directive("binsListItem", binsListItem)
.directive("taskList", taskList)
.directive("taskListItem", taskListItem)
.directive("trashListItem", trashListItem)

.directive("modalWindow", modalWindow)
.directive("controlPanel", controlPanel)
.directive("binForm", binForm)
.directive("searchForm", searchForm)
.directive("settingsForm", settingsForm)
.directive("settingsChoice", settingsChoice)

