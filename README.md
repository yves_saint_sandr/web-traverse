## Web-Traverse ##
A small web-program made like a wrapper for my previous `traverse` project

# Installation

To install program run the following commands in porjects's folder
```
pip install requirments.txt
```

```
yarn install
```
And finally run webpack to compile js files:

```
yarn run webpack -p
```

# Celery
To keep track of Celery task sheduling run server with:
```
celery worker --app=Traverse.celery --loglevel=info -E
```

# Server
! PLease make sure you have nginx installed and properly configured:
In your nginx directory (/etc/nginx/sites-enabled/), please run:
```
sudo ln -s ~/$TRAVERSE/Traverse_nginx.conf /etc/nginx/sites-enabled/
```
where $TRAVERSE is the path to Traverse root directory. Then, to apply changes:
```
sudo /etc/init.d/nginx restart
```
To boot the server type:
```
uwsgi --socket Traverse.sock --module Traverse.wsgi --chmod-socket=666
```

# Migrations
Run migrations with
```
python manage.py makemigrations
```
```
python manage.py migrate
```

# Redis
Celery uses redis as a backend, so, to ensure that your redis-server workes correctly type:
```
redis-cli ping
```

Yoy should see `PONG` in your console
