# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import Bin, Setting

admin.site.register(Bin)
admin.site.register(Setting)