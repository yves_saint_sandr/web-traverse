import os
from setuptools import setup, find_packages

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "traverse",
    version = "0.0.1",
    author = "Alexander Svito",
    author_email = "alexandervirk@gmail.com",
    description = ("The second labwork"),
    keywords = "second labwork",
    packages=['app', 'models', 'models.files', 'models.trashbin'],
    data_files=[('app', ['app/defaults.json'])],
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Topic :: Utilities",
        ],
    entry_points={
        'console_scripts':
            ['traverse = app.start:start'],
        }
)