from django import forms
from models import Bin, Setting

class BinForm(forms.ModelForm):
	class Meta(object):
		model = Bin
		fields = ('name','primary', 'user')


class SettingForm(forms.ModelForm):
	class Meta(object):
		model = Setting
		fields = ('name','primary', 'trashbin', 'silent', 'dummy', 'force')
		
