from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<id>\d+?)/settings/$', views.settings, name='settings'),
    url(r'^(?P<id>\d+?)/remove/$', views.remove, name='remove'),
    url(r'^(?P<id>\d+?)/update/$', views.update, name='update'),
    url(r'^bins/$', views.bins, name='bins'),
    url(r'^new/$', views.new, name='new'),
    url(r'^change/$', views.change, name='change'),
]