from __future__ import unicode_literals
from django.db import models
from traverse.app.app import App
from django.contrib.auth.models import User
from data import JSON_SETTINGS
import json

from Traverse.manager import PrimaryManager


class Bin(models.Model):
	name = models.CharField(max_length=140, unique=True)
	primary = models.BooleanField()
	user = models.ForeignKey(User, related_name="bins")

	objects = PrimaryManager()

	def __str__(self):
		return self.name

	def as_json(self):
		return {
			"name": self.name,
			"primary": self.primary,
			"id": self.id,
		}

class Setting(models.Model):
	
	name = models.CharField(max_length=140)
	primary = models.BooleanField()
	trashbin = models.ForeignKey(Bin, related_name="settings")
	silent = models.BooleanField(default=False)
	force = models.BooleanField(default=False)
	dummy = models.BooleanField(default=False)

	objects = PrimaryManager()

	def __str__(self):
		return self.name

	def as_json(self):
		return {
			"name": self.name,
			"primary": self.primary,
			"trashbin": self.trashbin.id,
			"silent": self.silent,
			"force": self.force,
			"dummy": self.dummy,
			"id": self.id
		}
