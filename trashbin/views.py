from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from forms import BinForm, SettingForm
from models import Bin, Setting
from django.views.decorators.csrf import csrf_protect
from traverse.app.app import App


@login_required(login_url='/login/')
def index(request):
	return render(request, 'bins.html')

@login_required(login_url='/login/')
def bins(request):
	return JsonResponse([bin_.as_json() for bin_ in request.user.bins.all()], safe=False)

@login_required(login_url='/login/')
def new(request):
	if request.method == "POST":
		form = BinForm(request.POST)
		if form.is_valid():
			if form.cleaned_data["primary"]:
				Bin.objects.reset()
			form.save()
	return JsonResponse([bin_.as_json() for bin_ in request.user.bins.all()], safe=False)

@login_required(login_url='/login/')	
def change(request):
	if request.method == "POST":
		new = Bin.objects.get(name=request.POST["new"]["name"])
		Bin.objects.primary = new
	return JsonResponse([bin_.as_json() for bin_ in request.user.bins.all()], safe=False)

@csrf_protect
def settings(request, id):
	if request.method == "GET":
		bin_ = Bin.objects.get(id=id)
		if not bin_.settings.exists():
			bin_.settings.create(name="Default", primary=True, silent=False)
		return JsonResponse([setting.as_json() for setting in bin_.settings.all()], safe=False)
	elif request.method == "POST":
		bin_ = Bin.objects.get(id=id)
		print(request.POST)
		if request.POST["form"]["public_name"] != "~/~ Create New One ~/~":
			bin_.settings.primary = Setting.objects.get(id=request.POST["selected"])
			setting = bin_.settings.primary
			form = SettingForm(request.POST["form"], instance=setting)
			if form.is_valid():
				if form.cleaned_data["primary"]:
					bin_.settings.reset()
				form.save()
				setting = bin_.settings.primary
				return JsonResponse(setting.as_json())
			else:
				return JsonResponse({"status": "FAILURE"})
		else:
			form = SettingForm(request.POST["form"])
			if form.is_valid():
				if form.cleaned_data["primary"]:
					bin_.settings.reset()
				form.save()
				setting = bin_.settings.primary
				return JsonResponse(setting.as_json())
			print(form.errors)
			return JsonResponse({"status": "FAILURE"})

def remove(request, id):
	bin_ = Bin.objects.get(id=id)
	app = App(bin_.name)
	app.dispose("*")
	bin_.delete()
	return JsonResponse({"status": "SUCCESS"})

def update(request, id):
	if request.method == "POST":
		bin_ = Bin.objects.get(id=id)
		bin_.name = request.POST["name"]
		bin_.save()
		return JsonResponse({"name": bin_.name})
