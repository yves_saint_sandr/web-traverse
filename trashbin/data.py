JSON_SETTINGS = """
{
	"app_settings": {
		"app": "traverse",
		"version": "1.0"
	},
	"bin_settings": {
		"folder_name": "Bin",
		"display_with": "less",
		"max_size": "1024",
		"delete_order": {
			"by": "deletion_date",
			"order": "descending"
		},
		"errors": {
			"on_delete": {
				"overflow": "error"
			},
			"on_restore": {
				"overflow": "error",
				"folder_missing": "ask",
				"name_duplicates": "ask"
			}
		}
	},
	"trash_settings": {
		"before_action": "skip",
		"size": "compress",
		"errors": {
			"access": "error",
			"name_duplicates": "inc"
		}
	}
}
"""
