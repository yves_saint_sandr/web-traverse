const path = require('path');
const webpack = require('webpack');

module.exports = {
  context: path.resolve(__dirname, './static/src/javascript'),
  entry: {
    app: './main.js',
  },
  output: {
    path: path.resolve(__dirname, './static/dist/javascript'),
    filename: '[name].bundle.js',
  },
};