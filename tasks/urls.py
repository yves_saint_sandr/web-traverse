from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'^(?P<id>\w+?)/cancel/$', views.cancel, name='cancel'),
	url(r'^(?P<page>\d+?)/$', views.paginate, name='paginate'),
    url(r'^new/$', views.new, name='new'),
    url(r'^restore/$', views.restore, name='restore'),
    url(r'^dispose/$', views.dispose, name='dispose'),
    url(r'^inspect/(?P<name>.+?)/$', views.inspect, name='inspect'),
]