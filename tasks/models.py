# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from traverse.app.app import App
from celery.decorators import task
from celery.result import AsyncResult
from celery.task.control import revoke

from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

from Traverse.manager import PaginateManager

import datetime, logging

@task(name="remove_file")
def _remove(app, file):
	app.remove(file)

@task(name="restore_file")
def _restore(app, file):
	app.restore(file)

@task(name="dispose_file")
def _dispose(app, file):
	app.dispose(file)




class Task(models.Model):
	
	user = models.ForeignKey(User, related_name="tasks")
	created_at = models.DateTimeField(auto_now_add=True)
	celery_id = models.CharField(null=True, max_length=256)
	file_name = models.CharField(max_length=256)
	action = models.CharField(max_length=64)
	public = models.BooleanField(default=True)

	objects = PaginateManager()

	class Meta(object):

		ordering = ['-created_at']
			

	def make_app(self):
		bin_name = self.user.bins.primary.name
		settings = self.user.bins.primary.settings.primary
		app = App(bin_name)
		if settings.silent:
			print("CHANGING VERBOSITY")
			app.verbosity = False
			self.public = False
			self.save()
		if settings.force:
			print("CHANGING FORCE")
			app.settings.access = "force"
		if settings.dummy:
			app.dummify = True
		return app



	@property
	def status(self):
		if not self.celery_id:
			return "FAILURE"
		if AsyncResult(self.celery_id).state == "PENDING" and self.created_at < timezone.now() - datetime.timedelta(hours=2):
			return "FORGOT"
		return AsyncResult(self.celery_id).state if self.celery_id else "FAILURE"

	def __str__(self):
		return "{} {}d {}".format(self.user,self.action, self.file_name)

	def run(self):
		app = self.make_app()
		if self.action == "remove":
			id_ = _remove.apply_async(args=[app, self.file_name]).task_id
		elif self.action == "restore":
			id_ = _restore.apply_async(args=[app, self.file_name]).task_id
		elif self.action == "dispose":
			id_ = _dispose.apply_async(args=[app, self.file_name]).task_id
		self.celery_id = id_
		self.save()
	
	def as_json(self):
		return {
			"id": self.id,
			"action": self.action,
			"created_at": self.created_at,
			"file_name": self.file_name,
			"status": self.status,
		}

	def cancel(self):
		print("="*80)
		print(self.celery_id)
		print("="*80)
		revoke(self.celery_id, terminate=True)