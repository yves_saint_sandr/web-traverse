# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required

from traverse.app.app import App

from models import Task

import os

def create_task(user, action, file):
	task = Task(
		user=user, 
		file_name=file,
		action=action
	)
	task.save()
	task.run()

@login_required(login_url='/login/')
def restore(request):
	if request.method == "POST":
		create_task(request.user, "restore", request.POST["descriptor"])
	return JsonResponse({"done": True})

@login_required(login_url='/login/')
def dispose(request):
	if request.method == "POST":
		create_task(request.user, "dispose", request.POST["descriptor"])
	return JsonResponse({"done": True})

@login_required(login_url='/login/')
def new(request):
	if request.method == "POST":
		if request.POST["length"] > 1:
			for file in request.POST.getlist("files"):	
				create_task(request.user, "remove", file["path"])
		else:
			file = request.POST["files"]
			create_task(request.user, "remove", file["path"])
	return JsonResponse({"done": True})

@login_required(login_url='/login/')
def inspect(request, name):
	app = App(name)
	return JsonResponse([trash.as_json() for trash in app.inspect()], safe=False)


@login_required(login_url='/login/')
def index(request):
	return render(request, 'tasks.html')

@login_required(login_url='/login/')
def paginate(request, page):
	return JsonResponse([task.as_json() for task in request.user.tasks.paginate(int(page))], safe=False)

def cancel(request, id):
	if request.method == "POST":
		Task.objects.get(id=id).cancel()
		return JsonResponse([task.as_json() for task in request.user.tasks.filter(public=True)], safe=False)